package ru.mihan.interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.mihan.interpreter.stack.PolishNotation;
import ru.mihan.interpreter.stack.StackComputer;
import ru.mihan.interpreter.stack.StackElement;

public class Main {

	public static void main(String[] args) {
		System.out.print("Enter expression: ");
		Scanner s = new Scanner (System.in);
		String line = s.nextLine();
		List <Token> tokens = new ArrayList<>();
		while (true) {
			line = line.trim();
			boolean found = false; 
			for (LexemType t : LexemType.values()){
				String value = t.GetMatchValue(line);
				if (value != null) {
					tokens.add(new Token(value, t));
					found = true;
					line = line.substring(value.length());
					break;
				}
			}
			if (!found){
				break;
			}
		}
		
		/*
		for (Token t : tokens) {
			System.out.println (t.Type + ": " + t.Value + ", ");
		}
		*/
		Parser p = new Parser(tokens);
		p.lang();
		System.out.println("Parsing has done");
		
		PolishNotation n = new PolishNotation();
		n.Parse(tokens);
		/*
		for (StackElement l : n.rpn) {
			System.out.print(l.token.Value + " ");
		}
		*/
		
		StackComputer comp = new StackComputer(n.rpn);
		comp.Compute();
	}
	
}
